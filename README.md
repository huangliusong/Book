# elasticSearch安装使用
### mac安装brew 
> brew install wget 

### wget下载elasticsearch

> wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.2.4.tar.gz

### 解压

> tar -zxvf elasticsearch-6.2.4.tar.gz 

### 进入目录
> cd elasticsearch-6.2.4

### 启动
> sh ./bin/elasticsearch

### 安装elasticsearch-head.

> https://github.com/mobz/elasticsearch-head.git

### 开启跨域
> vim /config/elasticsearch.yml

~~~
添加
http.cors.enabled: true
http.cors.allow-origin: "*"
~~~

## 分布式安装
### 1.设置master
~~~
http.cors.enabled: true
http.cors.allow-origin: "*"

cluster.name: liusong
node.name: master
node.master: true

network.host: 127.0.0.1

~~~

### 2.设置子节点1
~~~
cluster.name: liusong
node.name: liusong1

network.host: 127.0.0.1
http.port: 8200

discovery.zen.ping.unicast.hosts: ["127.0.0.1"]
~~~

### 3.设置子节点2
~~~
cluster.name: liusong
node.name: liusong1

network.host: 127.0.0.1
http.port: 8000

discovery.zen.ping.unicast.hosts: ["127.0.0.1"]
~~~


## 使用
### 创建结构
~~~
127.0.0.1:9200/people

{
  "settings": {
    "number_of_shards": 3,
    "number_of_replicas": 1
  },
  "mappings": {
    "man": {
      "properties": {
        "name": {
          "type": "text"
        },
        "country": {
          "type": "keyword"
        },
        "age": {
          "type": "integer"
        },
        "date": {
          "type": "date",
          "format": "yyyy-MM-dd HH:mm:ss||yyy-MM-dd||epoch_millis"
        }
      }
    }
  },
  "woman": {
  }
}
~~~
### 插入数据
~~~
方法：PUT
请求地址：127.0.0.1:9200/people/man/1
{
	"name":"黄柳淞",
	"country":"中国",
	"age":30,
	"date":"1921-2-2"
}
~~~
### 不带id的插入数据
~~~
方法：POST
请求地址：127.0.0.1:9200/people/man/
{
	"name":"黄柳淞",
	"country":"中国",
	"age":30,
	"date":"1921-2-2"
}
~~~

### 修改数据
~~~
方法：POST
请求地址：127.0.0.1:9200/people/man/1/_update
{
	"doc":{
		"name":"修改个name"
	}
}
~~~

### 脚本
~~~
方法：POST
请求地址：127.0.0.1:9200/people/man/1/_update
{
	"script":{
		"lang":"painless",
		"inline":"ctx._source.age+=10"
	}
}
或者
{
	"script":{
		"lang":"painless",
		"inline":"ctx._source.age=params.age",
		"params":{
			"age":100
		}
	}
}
~~~

### 删除
#### 删除文档
~~~
方法：DELETE
127.0.0.1:9200/people/man/1
~~~
#### 删除索引
~~~
127.0.0.1:9200/people
方法：DELETE
~~~

### 常用查询
#### 简单查询
#### 条件查询
~~~
1.全部查询
method：POST
url:127.0.0.1:9200/people/_search
{
	"query":{
		"match_all":{
			
		}
	}
}

2.
method：POST
url:127.0.0.1:9200/people/_search
{
	"query":{
		"match_all":{
			
		}
	},
	"from":1,
	"size":1
}

3.匹配名字
method：POST
url:127.0.0.1:9200/people/_search

{
	"query":{
		"match":{
			"name":"黄柳淞"
		}
	}
}
~~~
#### 聚合查询

# 预览
## 查询成功截图
![](/images/name.jpg)
## 启动

![](/images/1321ACD6-49BE-41B4-9557-FF6666A3A4A7.png)


1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
